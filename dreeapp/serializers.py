from rest_framework import serializers
from . import models
from .models import TreeBlank


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Region
        fields = '__all__'


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.District
        fields = '__all__'


class MahallaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Mahalla
        fields = '__all__'


class TreeTypeSeriliazer(serializers.ModelSerializer):
    class Meta:
        model = models.TreeType
        fields = '__all__'


class TreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tree
        fields = '__all__'


class TreeBlankSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TreeBlank
        fields = ['id', 'amount', 'tree', ]


class BlankSerializer(serializers.ModelSerializer):
    trees = TreeBlankSerializer(many=True)

    class Meta:
        model = models.Blank
        fields = ['region', 'district', 'mahalla', 'social_media_url', 'payment_type', 'trees', ]

    def create(self, validated_data):
        trees = validated_data.pop('trees')
        blank = models.Blank.objects.create(**validated_data)
        sum = 0
        for tree in trees:
            print(tree['amount'])
            treeblank = models.TreeBlank.objects.create(**tree, blank=blank)
            treeblank.save()

        return blank


class LegalSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.LeganEntity
        fields = '__all__'


class MostPlantedSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MostPlanted
        fields = '__all__'


class IndexSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Index
        fields = '__all__'


class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Menu
        exclude = ['footer', 'header']


class FooterSerializer(serializers.ModelSerializer):
    menu_header = MenuSerializer(many=True)
    menu_footer = MenuSerializer(many=True)

    class Meta:
        model = models.Footer
        exclude = ['address_link']


class RegionStatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RegionStatistics
        fields = ['id', 'region', 'image', 'donated_people', 'planted_trees', 'on_plan_planting']


class StaticPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StaticPage
        exclude = ['created_at','updated_at']


class ContactFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ContactForm
        exclude = ['created_at']


class FooterSerializer2(serializers.ModelSerializer):

    class Meta:
        model = models.Footer
        fields = ['phone', 'mail', 'full_address', 'address_link']


class ConnectionSerializer(serializers.ModelSerializer):
    footer = FooterSerializer2()

    class Meta:
        model = models.Index
        fields = ['s2_description','s2_description_uz','s2_description_en','s2_description_ru', 's3_title','s3_title_uz','s3_title_en','s3_title_ru', 's3_btn_text','s3_btn_text_uz','s3_btn_text_en','s3_btn_text_ru', 's3_slug','s3_slug_uz','s3_slug_en','s3_slug_ru', 'footer']
