import threading

from rest_framework import generics, views
from .serializers import *
from .models import *
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from django.db.models import Sum
from .utils import get_feed, telegram, facebook


class TreeTypeListView(generics.ListAPIView):
    queryset = TreeType.objects.all()
    serializer_class = TreeTypeSeriliazer


class TreeByTypeView(generics.ListAPIView):
    queryset = Tree.objects.all()
    serializer_class = TreeSerializer

    def get_object(self, pk):
        try:
            return Tree.objects.filter(type=pk)
        except Tree.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):

        trees = self.get_object(pk)
        serializer = TreeSerializer(trees, many=True)
        return Response(serializer.data)


class RegionListView(generics.ListAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class DistrictByRegion(views.APIView):
    def get_object(self, pk):
        try:
            return District.objects.filter(region=pk)
        except District.DoesNotExist:
            return Http404

    def get(self, request, pk, format=None):
        shaharlar = self.get_object(pk)
        serializer = DistrictSerializer(shaharlar, many=True)
        return Response(serializer.data)


class MahallaByDistrict(views.APIView):
    def get_object(self, pk):
        try:
            return Mahalla.objects.filter(district=pk)
        except Mahalla.DoesNotExist:
            return Http404

    def get(self, request, pk, format=None):
        mahalla = self.get_object(pk)
        serializer = MahallaSerializer(mahalla, many=True)
        return Response(serializer.data)


class SocialMediaInstagram(threading.Thread):
    def __init__(self, username, type, account_link, tree_amount):
        self.username = username
        self.type = type
        self.account_link = account_link
        self.tree_amount = tree_amount
        threading.Thread.__init__(self)

    def run(self):
        get_feed(self.username, self.type, self.account_link, self.tree_amount)


class SocialMedieTelegram(threading.Thread):
    def __init__(self, username, type, data, count):
        self.username = username
        self.type = type
        self.data = data
        self.count = count
        threading.Thread.__init__(self)

    def run(self):
        telegram(self.username, self.type, self.data, self.count)


class SocialMedieFacebook(threading.Thread):
    def __init__(self, username, data, count):
        self.username = username
        self.data = data
        self.count = count
        threading.Thread.__init__(self)

    def run(self):
        facebook(self.username, self.data, self.count)


class BlankPostView(generics.CreateAPIView):
    serializer_class = BlankSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        statistics_region = RegionStatistics.objects.get(region_id=serializer.data['region'])
        count = 0
        for i in serializer.data['trees']:
            count += i['amount']

        statistics_region.donated_trees += count
        statistics_region.donated_people += 1
        statistics_region.save(update_fields=['donated_people', 'donated_trees'])

        data = serializer.data['social_media_url']
        username = data.split('/')[3]

        if data.split('/')[2] == 'www.instagram.com':
            SocialMediaInstagram(username, 1, data, count).start()
            # get_feed(username=username, type=1, account_link=data, tree_amount=count)

        if data.split('/')[2] == "t.me":
            SocialMedieTelegram(username, 4, data, count).start()

        if data.split('/')[2] == "www.facebook.com":
            SocialMedieFacebook(username, data, count).start()

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


# pagination
class StandardResultsSetPagination(PageNumberPagination):
    page_size = 45
    page_size_query_param = 'page_size'


class LegalEntityView(generics.ListAPIView):
    queryset = LeganEntity.objects.all()
    serializer_class = LegalSerializer
    # pagination_class = StandardResultsSetPagination


class MostPlantedView(generics.ListAPIView):
    queryset = MostPlanted.objects.all()
    serializer_class = MostPlantedSerializer
    pagination_class = StandardResultsSetPagination

    def get(self, request):
        queryset = MostPlanted.objects.exclude(image='').order_by('-tree_amount')[:2]
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class IndexView(generics.RetrieveAPIView):
    queryset = Index.objects.all()
    serializer_class = IndexSerializer

    def get_object(self):
        instance = self.queryset.first()

        return instance


class MenuFooterApiView(generics.RetrieveAPIView):
    queryset = Footer.objects.all()
    serializer_class = FooterSerializer

    def get_object(self):
        instance = self.queryset.last()
        instance.menu_header = Menu.objects.filter(header=True)
        instance.menu_footer = Menu.objects.filter(footer=True)
        return instance


class RegionStatisticsApi(generics.ListAPIView):
    queryset = RegionStatistics.objects.all()
    serializer_class = RegionStatisticsSerializer


class StatisticsApi(generics.ListAPIView):
    queryset = RegionStatistics.objects.all()
    serializer_class = RegionStatisticsSerializer

    def get(self, request):
        x = {'donated_people': RegionStatistics.objects.aggregate(Sum('donated_people'))['donated_people__sum'],
             'donated_trees': RegionStatistics.objects.aggregate(Sum('donated_trees'))['donated_trees__sum'],
             'planted_trees': RegionStatistics.objects.aggregate(Sum('planted_trees'))['planted_trees__sum'],
             'on_plan_planting': RegionStatistics.objects.aggregate(Sum('on_plan_planting'))['on_plan_planting__sum']}
        return Response(x)


# static

class StaticPageView(generics.ListAPIView):
    queryset = StaticPage.objects.all()
    serializer_class = StaticPageSerializer


class StaticPageDetailView(generics.RetrieveAPIView):
    queryset = StaticPage
    serializer_class = StaticPageSerializer
    lookup_field = "slug"


class ContactFormView(generics.CreateAPIView):
    serializer_class = ContactFormSerializer


class ConnectionView(generics.RetrieveAPIView):
    queryset = Index.objects.all()
    serializer_class = ConnectionSerializer

    def get_object(self):
        instance = self.queryset.last()
        instance.footer = Footer.objects.last()

        return instance
