from modeltranslation.translator import register,TranslationOptions
from . import models

@register(models.Menu)
class MenuTranslationOptions(TranslationOptions):
    fields = ('title','slug')


@register(models.Footer)
class FooterTranslationOptions(TranslationOptions):
    fields = ('full_address','copyright')



@register(models.Region)
class RegionTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(models.District)
class DistrictTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(models.Mahalla)
class MahallaTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(models.TreeType)
class FooterTranslationOptions(TranslationOptions):
    fields = ('title',)



@register(models.Tree)
class TreeTranslationOptions(TranslationOptions):
    fields = ('title',)



@register(models.LeganEntity)
class LeganEntityTranslationOptions(TranslationOptions):
    fields = ('name',)



@register(models.MostPlanted)
class MostPlantedTranslationOptions(TranslationOptions):
    fields = ('username',)



@register(models.Index)
class IndexTranslationOptions(TranslationOptions):
    fields = ('s1_title','s1_description','s1_slug','s1_btn_text','s2_title','s2_description','s2_btn_text','s2_slug','s3_title','s3_description','s3_btn_text','s3_slug',)


@register(models.StaticPage)
class StaticPageTranslationOptions(TranslationOptions):
    fields = ('title','content','slug',)


@register(models.ContactForm)
class ContactFormTranslationOptions(TranslationOptions):
    fields = ('name','text')
